﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 11/13/2014
-- Description:	Selects rows from the Products table based on PackageId from the ProductXPackage table.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableProductXPackageByPackageId]
	@PackageId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT P.*
	FROM [dbo].[ProductXPackage] as PxP
	JOIN [dbo].[Product] as P
	ON PxP.[ProductId] = P.[ProductId]
	WHERE PxP.[PackageId] = @PackageId
END