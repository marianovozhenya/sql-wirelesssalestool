﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 10/8/2014
-- Description:	Selects all the rows from the EnumProductMarket table.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEnumProductMarketsAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[EnumProductMarket]
END