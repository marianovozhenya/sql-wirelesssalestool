﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/15/2014
-- Description:	Selects all the rows from the EnumProductCategory table.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEnumProductCategoriesAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[EnumProductCategory]
	ORDER BY (CASE SystemName
	WHEN 'Service' THEN 1
	WHEN 'VoIP' THEN 2
	WHEN 'VoiceAddOns' THEN 3
	WHEN 'Storage' THEN 4
	WHEN 'Installation' THEN 0
	WHEN 'WirelessRouter' THEN 5
	WHEN 'Term' THEN 6
	ELSE 0 END);
END