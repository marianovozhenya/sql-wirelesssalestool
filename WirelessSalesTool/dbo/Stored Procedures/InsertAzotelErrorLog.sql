﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 8/4/2014
-- Description:	Inserts a row into the AzotelErrorLog table.
-- =============================================
CREATE PROCEDURE [dbo].[InsertAzotelErrorLog]
	@CustomerId					INT,
	@ServiceId					INT,
	@PaymentProfileId			INT,
	@ServiceAddressId			INT,
	@BillingAddressId			INT,
	@AzotelId					INT,
	@Error						VARCHAR(MAX),
	@MethodName					VARCHAR(255),
	@CreatedByUserId			INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[AzotelErrorLog]
           ([CustomerId]
           ,[ServiceId]
           ,[PaymentProfileId]
           ,[ServiceAddressId]
           ,[BillingAddressId]
           ,[AzotelId]
           ,[Error]
           ,[CreatedAtUtc]
           ,[CreatedByUserId]
		   ,[AzotelAuthorizeNetToken]
		   ,[MethodName])
	SELECT
           @CustomerId
           ,@ServiceId
           ,@PaymentProfileId
           ,@ServiceAddressId
           ,@BillingAddressId
           ,@AzotelId
           ,@Error
           ,GETUTCDATE()
           ,@CreatedByUserId
		   ,AzotelAuthorizeNetToken
		   ,@MethodName
	FROM [dbo].[Customer] WHERE CustomerId = @CustomerId

	SELECT SCOPE_IDENTITY()
END