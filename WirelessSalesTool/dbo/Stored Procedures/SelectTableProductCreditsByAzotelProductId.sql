﻿-- =============================================
-- Author:		John Rubin
-- Create date: 01/16/2015
-- Description:	Selects a product credit by azotel product id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableProductCreditsByAzotelProductId] 
	-- Add the parameters for the stored procedure here
	@AzotelProductId int
AS
BEGIN
	SELECT CreditId
	FROM [dbo].[ProductCredit]
	WHERE RelatedAzotelProductId = @AzotelProductId
END