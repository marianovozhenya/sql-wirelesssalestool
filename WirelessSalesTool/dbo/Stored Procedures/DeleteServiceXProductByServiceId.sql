﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/17/2014
-- Description:	Deletes a row from ServiceXProduct by service id.
-- =============================================
CREATE PROCEDURE [dbo].[DeleteServiceXProductByServiceId]
	@ServiceId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[ServiceXProduct] WHERE ServiceId = @ServiceId
END