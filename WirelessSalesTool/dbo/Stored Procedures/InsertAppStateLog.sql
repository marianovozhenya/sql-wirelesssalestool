﻿-- =============================================
-- Author:		John Rubin
-- Create date: 02/24/2015
-- Description:	Inserts an app state log.
-- =============================================
CREATE PROCEDURE [dbo].[InsertAppStateLog] 
	-- Add the parameters for the stored procedure here
	@UserName varchar(255),
	@UserEmail varchar(255),
	@ApplicationState varchar(255),
	@RequestStatus varchar(255),
	@Message text,
	@TimeStamp datetime,
	@RequestType varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO AppStateLog(UserName, UserEmail, ApplicationState, RequestStatus, [Message], [TimeStamp], RequestType)
	VALUES(@UserName, @UserEmail, @ApplicationState, @RequestStatus, @Message, @TimeStamp, @RequestType)

	SELECT SCOPE_IDENTITY()
END