﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 11/13/2014
-- Description:	Selects rows from the Package table by EnumProductMarketId.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTablePackagesByEnumProductMarketId]
	@EnumProductMarketId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Package]
	WHERE [EnumProductMarketId] = @EnumProductMarketId
END