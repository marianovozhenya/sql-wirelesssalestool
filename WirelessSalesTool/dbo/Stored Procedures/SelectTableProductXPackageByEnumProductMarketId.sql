﻿-- =============================================
-- Author:		John Rubin
-- Create date: 11/14/2014
-- Description:	Gets all ProductXEnumPackageIds and su
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableProductXPackageByEnumProductMarketId] 
	-- Add the parameters for the stored procedure here
	@EnumProductMarketId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT px.* 
	FROM ProductXPackage as px join Package as p on px.PackageId = p.PackageId 
	WHERE p.EnumProductMarketId = EnumProductMarketId
END