﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 10/8/2014
-- Description:	Selects rows from the Product table by EnumProductMarketId.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableProductsByEnumProductMarketId]
	@EnumProductMarketId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Product]
	WHERE [EnumProductMarketId] = @EnumProductMarketId
	ORDER BY [Product].DisplayOrder, [Product].Price
END