﻿CREATE TABLE [dbo].[CustomerContract] (
    [CustomerContractId]  INT           IDENTITY (1, 1) NOT NULL,
    [Submitter]           VARCHAR (255) CONSTRAINT [DF_CustomerContract_Submitter_Unknown] DEFAULT ('Unknown') NOT NULL,
    [NocPeriod]           INT           CONSTRAINT [DF_CustomerContract_NocPeriod_30] DEFAULT ((30)) NOT NULL,
    [AzotelCustomerId]    INT           NULL,
    [AzotelCustomerEmail] VARCHAR (255) NOT NULL,
    [RepEmail]            VARCHAR (255) NOT NULL,
    [AgreementId]         VARCHAR (MAX) NOT NULL,
    [SignatureStatus]     INT           NOT NULL,
    [DateIssued]          DATETIME      NOT NULL,
    CONSTRAINT [PK_CustomerContract_CustomerContractId] PRIMARY KEY CLUSTERED ([CustomerContractId] ASC)
);

