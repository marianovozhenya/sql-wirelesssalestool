﻿CREATE TABLE [dbo].[ProductXPackage] (
    [ProductXPackageId] INT IDENTITY (1, 1) NOT NULL,
    [PackageId]         INT NULL,
    [ProductId]         INT NULL,
    CONSTRAINT [PK_ProductXPackage_ProductXPackageId] PRIMARY KEY CLUSTERED ([ProductXPackageId] ASC),
    CONSTRAINT [FK_Product_ProductId] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[Product] ([ProductId])
);

