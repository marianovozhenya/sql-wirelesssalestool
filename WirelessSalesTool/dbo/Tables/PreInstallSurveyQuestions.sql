﻿CREATE TABLE [dbo].[PreInstallSurveyQuestions] (
    [QuestionId]      INT          IDENTITY (1, 1) NOT NULL,
    [QuestionText]    TEXT         CONSTRAINT [DF__PreInstal__Quest__6FE99F9F] DEFAULT ('What''s your question?') NOT NULL,
    [QuestionSubText] TEXT         NULL,
    [FieldType]       VARCHAR (50) NOT NULL,
    [Active]          BIT          CONSTRAINT [DF__PreInstal__Activ__70DDC3D8] DEFAULT ((1)) NOT NULL,
    [Order]           INT          NULL,
    CONSTRAINT [PK__PreInsta__0DC06FACE0232E4A] PRIMARY KEY CLUSTERED ([QuestionId] ASC)
);

