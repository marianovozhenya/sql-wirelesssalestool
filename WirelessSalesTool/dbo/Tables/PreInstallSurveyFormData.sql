﻿CREATE TABLE [dbo].[PreInstallSurveyFormData] (
    [FormDataId]  INT  IDENTITY (1, 1) NOT NULL,
    [FormEntryId] INT  NOT NULL,
    [QuestionId]  INT  NOT NULL,
    [FieldId]     INT  NOT NULL,
    [FieldValue]  TEXT NULL,
    CONSTRAINT [PK__PreInsta__6B395ADD0797B516] PRIMARY KEY CLUSTERED ([FormDataId] ASC),
    CONSTRAINT [FK__PreInstallSurveyFormEntries_FormEntryId_PreInstallSurveyFormData_FormEntryId] FOREIGN KEY ([FormEntryId]) REFERENCES [dbo].[PreInstallSurveyFormEntries] ([FormEntryId]),
    CONSTRAINT [FK__PreInstallSurveyQuestions_QuestionId_PreInstallSurveyFormData_QuestionId] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[PreInstallSurveyQuestions] ([QuestionId]),
    CONSTRAINT [FK_PreInstallSurveyFormFields_FieldId_PreInstallSurveyFormData_FieldId] FOREIGN KEY ([FieldId]) REFERENCES [dbo].[PreInstallSurveyFields] ([FieldId])
);

