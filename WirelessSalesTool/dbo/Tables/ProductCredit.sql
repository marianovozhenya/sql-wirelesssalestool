﻿CREATE TABLE [dbo].[ProductCredit] (
    [ProductCreditId]        INT IDENTITY (1, 1) NOT NULL,
    [CreditId]               INT NOT NULL,
    [RelatedAzotelProductId] INT NOT NULL,
    CONSTRAINT [pk_productCreditId] PRIMARY KEY CLUSTERED ([ProductCreditId] ASC)
);

