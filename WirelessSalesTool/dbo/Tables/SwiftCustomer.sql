﻿CREATE TABLE [dbo].[SwiftCustomer] (
    [SwiftCustomerId]     INT             IDENTITY (1, 1) NOT NULL,
    [CardValue]           DECIMAL (10, 2) NULL,
    [CustomerName]        VARCHAR (255)   NULL,
    [CustomerEmail]       VARCHAR (100)   NULL,
    [SubmitterEmail]      VARCHAR (255)   NULL,
    [AzotelCustomerId]    INT             NULL,
    [ReferringCustomerId] INT             NULL,
    [PaymentReferenceId]  VARCHAR (255)   NULL,
    [IssuedAtUtc]         DATETIME        NULL,
    CONSTRAINT [PK__SwiftCus__5B2352CF3D600FB7] PRIMARY KEY CLUSTERED ([SwiftCustomerId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [I_ReferringCustomerId]
    ON [dbo].[SwiftCustomer]([ReferringCustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [I_AzotelCustomerId]
    ON [dbo].[SwiftCustomer]([AzotelCustomerId] ASC);


GO
CREATE NONCLUSTERED INDEX [I_CardValue]
    ON [dbo].[SwiftCustomer]([CardValue] ASC);

