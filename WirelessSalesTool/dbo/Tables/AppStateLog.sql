﻿CREATE TABLE [dbo].[AppStateLog] (
    [AppStateLogId]    INT           IDENTITY (1, 1) NOT NULL,
    [UserName]         VARCHAR (100) NOT NULL,
    [UserEmail]        VARCHAR (150) NOT NULL,
    [RequestStatus]    VARCHAR (30)  NOT NULL,
    [Message]          TEXT          NULL,
    [TimeStamp]        DATETIME      NOT NULL,
    [RequestType]      VARCHAR (255) NOT NULL,
    [ApplicationState] VARCHAR (MAX) NULL,
    CONSTRAINT [PK__AppState__7D724FB361AA7AB6] PRIMARY KEY CLUSTERED ([AppStateLogId] ASC)
);

