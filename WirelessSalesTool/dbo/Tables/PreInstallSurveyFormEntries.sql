﻿CREATE TABLE [dbo].[PreInstallSurveyFormEntries] (
    [FormEntryId]        INT           IDENTITY (1, 1) NOT NULL,
    [Timestamp]          DATETIME      CONSTRAINT [DF__PreInstal__Times__76969D2E] DEFAULT (getutcdate()) NOT NULL,
    [SubmitterId]        INT           NOT NULL,
    [SubmitterName]      VARCHAR (255) NOT NULL,
    [SalesmanAzotelId]   INT           NOT NULL,
    [SalesmanName]       VARCHAR (255) NOT NULL,
    [CustomerAzotelId]   INT           NOT NULL,
    [CustomerAzotelName] VARCHAR (255) NOT NULL,
    CONSTRAINT [PK__PreInsta__414A4467FC667881] PRIMARY KEY CLUSTERED ([FormEntryId] ASC)
);

