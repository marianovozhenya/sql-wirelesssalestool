﻿CREATE TABLE [dbo].[PreInstallSurveyFields] (
    [FieldId]    INT IDENTITY (1, 1) NOT NULL,
    [QuestionId] INT NOT NULL,
    [Order]      INT NULL,
    CONSTRAINT [PK__Field__C8B6FF0761698B6A] PRIMARY KEY CLUSTERED ([FieldId] ASC),
    CONSTRAINT [FK__Field__QuestionI__73BA3083] FOREIGN KEY ([QuestionId]) REFERENCES [dbo].[PreInstallSurveyQuestions] ([QuestionId])
);

