﻿-- =============================================
-- Author:		Andrew Larsson
-- Create date: 9/25/2014
-- Description:	Gets a User by User Identifier.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowUserByUserIdentifier]
	@EnumUserIdentifierType	INT,
	@Identifier				VARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		[User].*
	FROM
		[dbo].[User] JOIN
			[dbo].[UserIdentifier]
				ON [UserIdentifier].UserId = [User].UserId
	WHERE
		[UserIdentifier].EnumUserIdentifierTypeId = @EnumUserIdentifierType AND
		[UserIdentifier].Identifier = @Identifier
END