﻿CREATE TABLE [dbo].[EnumUserIdentifierType] (
    [EnumUserIdentifierTypeId] INT           NOT NULL,
    [SystemName]               VARCHAR (50)  NOT NULL,
    [DisplayName]              VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumUserIdentifierType_EnumUserIdentifierTypeId] PRIMARY KEY CLUSTERED ([EnumUserIdentifierTypeId] ASC)
);

