﻿CREATE TABLE [dbo].[UserIdentifier] (
    [UserIdentifierId]         INT           IDENTITY (1, 1) NOT NULL,
    [UserId]                   INT           NOT NULL,
    [EnumUserIdentifierTypeId] INT           NOT NULL,
    [Identifier]               VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_UserIdentifier_UserIdentifierId] PRIMARY KEY CLUSTERED ([UserIdentifierId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_UserIdentifier_EnumUserIdentifierTypeId_Identifier]
    ON [dbo].[UserIdentifier]([EnumUserIdentifierTypeId] ASC, [Identifier] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_UserIdentifier_EnumUserIdentifierTypeId_UserId]
    ON [dbo].[UserIdentifier]([UserId] ASC, [EnumUserIdentifierTypeId] ASC);

