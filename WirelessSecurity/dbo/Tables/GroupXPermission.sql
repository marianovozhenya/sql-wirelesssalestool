﻿CREATE TABLE [dbo].[GroupXPermission] (
    [GroupXPermissionId] INT IDENTITY (1, 1) NOT NULL,
    [GroupId]            INT NOT NULL,
    [PermissionId]       INT NOT NULL,
    CONSTRAINT [PKCX_GroupXPermission_GroupXPermissionId] PRIMARY KEY CLUSTERED ([GroupXPermissionId] ASC)
);

