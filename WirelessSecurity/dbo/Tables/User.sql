﻿CREATE TABLE [dbo].[User] (
    [UserId]       INT           IDENTITY (1, 1) NOT NULL,
    [FirstName]    VARCHAR (50)  NOT NULL,
    [LastName]     VARCHAR (50)  NOT NULL,
    [EmailAddress] VARCHAR (100) NULL,
    CONSTRAINT [PKCX_User_UserId] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

