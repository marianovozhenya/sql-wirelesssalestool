﻿CREATE TABLE [dbo].[Group] (
    [GroupId]                INT           IDENTITY (1, 1) NOT NULL,
    [ActiveDirectoryGroupId] VARCHAR (255) NULL,
    [Name]                   VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_Group_GroupId] PRIMARY KEY CLUSTERED ([GroupId] ASC)
);

