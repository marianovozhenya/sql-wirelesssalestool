﻿CREATE TABLE [dbo].[UserXGroup] (
    [UserXGroupId] INT IDENTITY (1, 1) NOT NULL,
    [UserId]       INT NOT NULL,
    [GroupId]      INT NOT NULL,
    CONSTRAINT [PKCX_UserXGroup_UserXGroupId] PRIMARY KEY CLUSTERED ([UserXGroupId] ASC)
);

