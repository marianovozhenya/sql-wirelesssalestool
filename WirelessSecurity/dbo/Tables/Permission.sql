﻿CREATE TABLE [dbo].[Permission] (
    [PermissionId] INT           NOT NULL,
    [SystemName]   VARCHAR (50)  NOT NULL,
    [DisplayName]  VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_Permission_PermissionId] PRIMARY KEY CLUSTERED ([PermissionId] ASC)
);

